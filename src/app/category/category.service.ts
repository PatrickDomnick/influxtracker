import { Injectable } from '@angular/core';
import { Item } from '../item/item';
import { Category } from './category';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {
  public categories: Category[];

  constructor() {
    this.categories = new Array<Category>();
    const categoryJson = localStorage.getItem('categories');
    if (categoryJson !== null) {
      this.categories = JSON.parse(categoryJson) as Category[];
    } else {
      const category = [
        new Category({
          name: 'Training',
          description: 'Sport and more',
          items: [
            new Item({
              name: 'Running',
              description: 'Running for 30 Minutes',
              url: 'https://material.angular.io/assets/img/examples/shiba2.jpg',
            }),
            new Item({
              name: 'HIT',
              description: 'HIT for 30 Minutes',
            }),
            new Item({
              name: 'Cycling',
              description: 'Cycling for 30 Minutes',
            }),
            new Item({
              name: 'Boulder',
              description: 'Boulder for 30 Minutes',
            }),
          ],
        }),
      ];
      localStorage.setItem('categories', JSON.stringify(category));
    }
  }

  save() {
    localStorage.setItem('categories', JSON.stringify(this.categories));
  }
}
