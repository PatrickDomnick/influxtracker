import { Item } from './../../item/item';
import { Category } from 'src/app/category/category';
import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-create-category',
  templateUrl: './create-category.component.html',
  styleUrls: ['./create-category.component.css'],
})
export class CreateCategoryComponent {
  public form = new FormGroup({
    name: new FormControl(''),
    description: new FormControl(''),
  });

  constructor(private dialogRef: MatDialogRef<CreateCategoryComponent>) {}

  public save() {
    const { name, description } = this.form.value;
    if (name) {
      this.dialogRef.close(
        new Category({
          name,
          description: description ?? undefined,
        })
      );
    }
  }
}
