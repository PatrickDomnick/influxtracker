import { Item } from '../item/item';

export interface CategoryProps {
  name: string;
  description?: string;
  items?: Item[];
}

export class Category {
  name: string;
  description?: string;
  items: Item[];

  constructor(props: CategoryProps) {
    this.name = props.name;
    this.description = props.description;
    this.items = props.items ?? new Array<Item>();
  }
}
