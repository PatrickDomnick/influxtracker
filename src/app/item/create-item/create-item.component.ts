import { Category } from './../../category/category';
import { Component, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Item } from '../item';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.component.html',
  styleUrls: ['./create-item.component.css'],
})
export class CreateItemComponent {
  public form = new FormGroup({
    name: new FormControl(''),
    description: new FormControl(''),
    url: new FormControl(''),
    unit: new FormControl(''),
  });

  constructor(
    private dialogRef: MatDialogRef<CreateItemComponent>,
    @Inject(MAT_DIALOG_DATA) private category: Category
  ) {}

  public save() {
    const { name, description, url, unit } = this.form.value;
    if (name) {
      this.category.items.push(
        new Item({
          name,
          description: description ?? undefined,
          url: url ?? undefined,
          unit: unit ?? undefined,
        })
      );
    }
    this.dialogRef.close();
  }
}
