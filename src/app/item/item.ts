export interface ItemProperties {
  name: string;
  description?: string;
  url?: string;
  unit?: string;
}

export class Item {
  name: string;
  description?: string;
  url?: string;
  unit?: string;

  constructor(props: ItemProperties) {
    this.name = props.name;
    this.description = props.description;
    this.url = props.url;
    this.unit = props.unit;
  }
}
