import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Category } from 'src/app/category/category';

@Component({
  selector: 'app-move-item',
  templateUrl: './move-item.component.html',
  styleUrls: ['./move-item.component.css'],
})
export class MoveItemComponent {
  constructor(
    private dialogRef: MatDialogRef<MoveItemComponent>,
    @Inject(MAT_DIALOG_DATA) public categories: Category[]
  ) {}

  public save(category: Category) {
    this.dialogRef.close(category);
  }
}
