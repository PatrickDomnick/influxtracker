import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Item } from '../item';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css'],
})
export class EditItemComponent {
  public form = new FormGroup({
    name: new FormControl(this.item.name),
    description: new FormControl(this.item.description),
    url: new FormControl(this.item.url),
    unit: new FormControl(this.item.unit),
  });

  constructor(
    private dialogRef: MatDialogRef<EditItemComponent>,
    @Inject(MAT_DIALOG_DATA) public item: Item
  ) {}

  public save() {
    this.dialogRef.close(this.form.value);
  }
}
