import { AuthenticationService } from './authentication.service';
import { Category, CategoryProps } from './category/category';
import { CategoryService } from './category/category.service';
import { Component, OnInit } from '@angular/core';
import { CreateCategoryComponent } from './category/create-category/create-category.component';
import { CreateItemComponent } from './item/create-item/create-item.component';
import { EditCategoryComponent } from './category/edit-category/edit-category.component';
import { EditItemComponent } from './item/edit-item/edit-item.component';
import { FormGroup, FormControl } from '@angular/forms';
import { Item } from './item/item';
import { MatDialog } from '@angular/material/dialog';
import { MoveItemComponent } from './item/move-item/move-item.component';
import { Point } from '@influxdata/influxdb-client';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  // Setup Form Control
  public setupForm = new FormGroup({
    url: new FormControl(''),
    token: new FormControl(''),
    organization: new FormControl(''),
    bucket: new FormControl(''),
  });

  // Submit Control
  public selectedItem?: Item = undefined;
  public selectedCategory?: Category = undefined;
  public submittingValue = new FormControl();
  public submitting = false;

  /**
   * Gets Data from Local Storage and initialize the Influx DB Connection
   * @param dialog
   */
  constructor(
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    public authService: AuthenticationService,
    public categoryService: CategoryService
  ) {}

  /**
   * Change default Tab
   */
  ngOnInit(): void {
    this.submittingValue.disable();
  }

  public selectCategory(category: Category): void {
    this.selectedItem = undefined;
    this.selectedCategory = category;
  }

  public selectItem(item: Item): void {
    this.selectedItem = item;
    this.submittingValue.enable();
    this.submittingValue.setValue(undefined);
  }

  public async submitData(): Promise<void> {
    if (this.authService.client && this.selectedCategory && this.selectedItem) {
      this.submitting = true;
      const point = new Point(this.selectedCategory.name).floatField(
        this.selectedItem.name,
        this.submittingValue.value
      );
      this.authService.client.writePoint(point);
      try {
        await this.authService.client?.flush();
        this.submittingValue.setValue(undefined);
        this.snackBar.open('Submitted Value', undefined, {
          duration: 1000,
        });
      } catch (error) {
        this.snackBar.open((error as Error).message, undefined, {
          duration: 1000,
        });
      }
      this.submitting = false;
    }
  }

  /**
   * Opens a Dialog to create a new Category
   * A name is required while a description is optional
   */
  public createCategory(categories: Category[]): void {
    const dialogRef = this.dialog.open(CreateCategoryComponent);
    dialogRef.afterClosed().subscribe((result: Category) => {
      if (result) {
        categories.push(result);
        this.categoryService.save();
      }
    });
  }

  /**
   * Opens a Dialog to edit the name and description of the category
   * CAUTION: Doing so will invalidate all acquired data from its items
   * @param category The Category to edit
   */
  public editCategory(categories: Category[], category: Category): void {
    const index = categories.indexOf(category);
    if (index > -1) {
      const dialogRef = this.dialog.open(EditCategoryComponent, {
        data: categories[index],
      });
      dialogRef.afterClosed().subscribe((result: CategoryProps) => {
        if (result) {
          categories[index] = {
            ...result,
            items: category.items,
          };
          this.categoryService.save();
        }
      });
    }
  }

  /**
   * Opens a Dialog to create a new Item in a Category
   * A name is required while a description and url is optional
   */
  public createItem(category: Category): void {
    const dialogRef = this.dialog.open(CreateItemComponent, {
      data: category,
    });
    dialogRef.afterClosed().subscribe(() => {
      this.categoryService.save();
    });
  }

  /**
   * Opens a Dialog to edit the name and description of the item
   * CAUTION: Doing so will invalidate all acquired data from this item
   * @param category The Category of the item
   * @param item The item that is about to be changed
   */
  public editItem(category: Category, item: Item): void {
    const index = category.items.indexOf(item);
    if (index > -1) {
      const dialogRef = this.dialog.open(EditItemComponent, {
        data: category.items[index],
      });
      dialogRef.afterClosed().subscribe((result: Item) => {
        if (result) {
          category.items[index] = result;
          this.categoryService.save();
        }
      });
    } else {
      console.warn('Something went wrong editing Item from Category');
    }
  }

  /**
   * Opens a Dialog to edit the name and description of the item
   * CAUTION: Doing so will invalidate all acquired data from this item
   * @param categories All Categories (Root Element)
   * @param category Current Category of the item
   * @param item The item which is about to be moved
   */
  public moveItem(
    categories: Category[],
    category: Category,
    item: Item
  ): void {
    const index = category.items.indexOf(item);
    if (index > -1) {
      const dialogRef = this.dialog.open(MoveItemComponent, {
        data: categories,
      });
      dialogRef.afterClosed().subscribe((result: Category) => {
        if (result) {
          // Remove from old Category
          category.items.splice(index, 1);
          // Add to new Category
          result.items.push(item);
          this.categoryService.save();
        }
      });
    } else {
      console.warn('Something went wrong editing Item from Category');
    }
  }

  /**
   * Opens a Dialog to edit the name and description of the item
   * CAUTION: Doing so will invalidate all acquired data from this item
   * @param categories All Categories (Root Element)
   * @param category Current Category of the item
   * @param item The item which is about to be moved
   */
  public deleteItem(category: Category, item: Item): void {
    const index = category.items.indexOf(item);
    if (index > -1) {
      category.items.splice(index, 1);
      this.categoryService.save();
    } else {
      console.warn('Something went wrong deleting Item from Category');
    }
  }

  /**
   * Save the Influx DB Connection String to local Storage
   */
  public save(): void {
    const authProps = this.setupForm.value;
    if (
      authProps.bucket &&
      authProps.organization &&
      authProps.token &&
      authProps.url
    ) {
      this.authService.toLocalStorage({
        bucket: authProps.bucket,
        organization: authProps.organization,
        token: authProps.token,
        url: authProps.url,
      });
    }
  }
}
