import { InfluxDB, WriteApi } from '@influxdata/influxdb-client';
import { Injectable } from '@angular/core';

export interface AuthenticationProperties {
  url: string;
  token: string;
  organization: string;
  bucket: string;
}

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  public client?: WriteApi;

  constructor() {
    const authenticationJson = localStorage.getItem('authentication');
    if (authenticationJson !== null) {
      const authenticationData = JSON.parse(
        authenticationJson
      ) as AuthenticationProperties;
      const { url, token, bucket, organization } = authenticationData;
      const influx = new InfluxDB({ url, token });
      this.client = influx.getWriteApi(organization, bucket);
    }
  }

  public toLocalStorage(props: AuthenticationProperties) {
    localStorage.setItem('authentication', JSON.stringify(props));
    const { url, token, bucket, organization } = props;
    const influx = new InfluxDB({ url, token });
    this.client = influx.getWriteApi(organization, bucket);
  }
}
